<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo('charset'); ?>">

	<title><?php wp_title(''); ?></title>
	
	<!-- Mobile viewport optimized: j.mp/bplateviewport -->
	<meta name="viewport" content="width=device-width" />
				
	<!-- Favicon and Feed -->
	<link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> Feed" href="<?php echo home_url(); ?>/feed/">
	
	<!--  iPhone Web App Home Screen Icon -->
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/images/devices/reverie-icon-ipad.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/images/devices/reverie-icon-retina.png" />
	<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/images/devices/reverie-icon.png" />
	
	<!-- Enable Startup Image for iOS Home Screen Web App -->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<link rel="apple-touch-startup-image" href="<?php echo get_template_directory_uri(); ?>/mobile-load.png" />

	<!-- Startup Image iPad Landscape (748x1024) -->
	<link rel="apple-touch-startup-image" href="<?php echo get_template_directory_uri(); ?>/images/devices/reverie-load-ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)" />
	<!-- Startup Image iPad Portrait (768x1004) -->
	<link rel="apple-touch-startup-image" href="<?php echo get_template_directory_uri(); ?>/images/devices/reverie-load-ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)" />
	<!-- Startup Image iPhone (320x460) -->
	<link rel="apple-touch-startup-image" href="<?php echo get_template_directory_uri(); ?>/images/devices/reverie-load.png" media="screen and (max-device-width: 320px)" />
	
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
	
	<header role="banner">
		<div class="strip-global">
			<div class="row">
				<div class="twelve columns">
					<?php wp_nav_menu(array('theme_location' => 'global_navigation', 'container' => false, 'menu_class' => 'global-nav eight columns')); ?>
				</div>
			</div>
		</div>
		
		<div class="bg-header">
			<div class="row">
				<div class="twelve columns">
					<a href="http://www.unimelb.edu.au/"><img class="logo" alt="<?php bloginfo('name'); ?>" src="<?php echo get_template_directory_uri(); ?>/images/logo.png" width="150" height="131"></a>
					<div class="site-header">
						<h4 class="subheader"><a href="http://www.lib.unimelb.edu.au/"><?php bloginfo('description'); ?></a></h4>
						<h1><a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>"><?php bloginfo('name'); ?></a></h1>
					</div>
				</div>
			</div>
		</div>

		<div class="nav-main">	
			<div class="row">	
				<div class="twelve columns">
					<nav role="navigation">
						<?php
							wp_nav_menu( array(
							'theme_location' => 'primary_navigation',
							'container' =>false,
							'menu_class' => '',
							'echo' => true,
							'before' => '',
							'after' => '',
							'link_before' => '',
							'link_after' => '',
							'depth' => 0,
							'items_wrap' => '<ul class="nav-bar">%3$s</ul>',
							'walker' => new reverie_walker())
						); ?>
					</nav>
				</div>
			</div>
		</div>
	</header>

	<?php if ( is_front_page() ) : ?>
		<div class="hero">
			<img src="<?php echo get_template_directory_uri(); ?>/images/hero.jpg">
		</div>
	<?php endif; ?>

	<div id="container" class="container" role="document">
		<!-- Row for main content area -->
		<div id="main" class="row">