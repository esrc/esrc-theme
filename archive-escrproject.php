<?php get_header(); ?>

		<!-- Row for main content area -->
		<div id="content" class="eight columns" role="main">
			
			<div class="project-intro">
				<?php if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p class="breadcrumbs">','</p>');
				} ?>
				<h1>Featured Projects</h1>
				<p><?php the_content(); ?></p>
			</div>
			
			<div class="info-box six columns">
				<?php $featured_project = new WP_Query( array(
					'post_type' => 'escrproject',
					'posts_per_page' => 1,
					'meta_key' => 'project_feature',
					'meta_value' => '1',
					'compare' => '=='
					));
				?>
				
				<?php while ( $featured_project->have_posts() ) : $featured_project->the_post(); ?>
					<?php if ( has_post_thumbnail() ) {?>
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
							<figure class="figure"><?php the_post_thumbnail( array( 'width' => 280, 'height' => 125, 'crop' => 'true' ) , array( 'class' => '' ) ); ?></figure>
						</a>
					<?php }?>
					<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
					<ul class="project-meta no-bullet">
						<li>
							<h3>Link</h3>
							<p><a href="<?php the_field('project_link'); ?>"><?php the_field('project_link'); ?></a></p>
						</li>
						<li>
							<h3>Partners</h3>
							<p><?php the_field('project_partners'); ?></p>
						</li>
						<li>
							<h3>Start</h3>
							<p><?php $date = DateTime::createFromFormat('Ymd', get_field('project_start_date'));
							echo $date->format('F Y'); ?></p>
						</li>
						
						<li>
							<h3>Staff</h3>
							<p><?php the_field('project_lead_staff'); ?></p>
						</li>
						<li>
							<h3>Description</h3>
							<p><?php the_content(); ?></p>
						</li>
					</ul>
				
				<?php endwhile; ?>
			</div>
			
			<div class="post-box six columns">
			
				<?php while (have_posts()) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<?php if ( has_post_thumbnail() ) {?>
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
								<?php the_post_thumbnail( array( 'width' => 87, 'height' => 87, 'crop' => 'true' ) , array( 'class' => 'alignleft' ) ); ?>
							</a>
						<?php }?>
						<div class="holder">
							<header>
								<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
							</header>
							<div class="entry-content">
								<?php the_excerpt(); ?>
							</div>
							
						</div>
					</article>	
				<?php endwhile; ?>
				
			</div>
			
			

		</div><!-- End Content row -->
		
		<?php get_sidebar(); ?>
		
<?php get_footer(); ?>