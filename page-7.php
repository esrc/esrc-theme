<?php get_header(); ?>

		<!-- Row for main content area -->
		<div id="content" class="eight columns" role="main">
	
			<div class="post-box">
				<?php if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p class="breadcrumbs">','</p>');
				} ?>
				<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>
				
				<ul class="block-grid mobile three-up featured-home">
					<?php $about_children = new WP_Query( array(
						'post_type' => 'page',
						'post_parent' => 7,
                                                'order' => 'asc',
						'orderby' => 'menu_order'
						));
					?>
					<?php while ( $about_children->have_posts() ) : $about_children->the_post(); ?>
						<li>
							<?php if ( has_post_thumbnail() ) {?>
								<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
									<?php the_post_thumbnail( array( 'width' => 190, 'height' => 97, 'crop' => 'true' ) , array( 'class' => '' ) ); ?>
								</a>
							<?php }?>
							<h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
							<?php the_excerpt();?>
						</li>
					<?php endwhile; ?>
				</ul>

			</div>

		</div><!-- End Content row -->
		
		<?php get_sidebar(); ?>
		
<?php get_footer(); ?>
