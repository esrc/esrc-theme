<h1>Featured Projects</h1>

<ul class="block-grid mobile three-up featured-home">
	<?php $home_projects = new WP_Query( array(
		'post_type' => 'escrproject',
		'posts_per_page' => 6
		));
	?>
	<?php while ( $home_projects->have_posts() ) : $home_projects->the_post(); ?>
		<li>
			<?php if ( has_post_thumbnail() ) {?>
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
					<?php the_post_thumbnail( array( 'width' => 190, 'height' => 97, 'crop' => 'true' ) , array( 'class' => '' ) ); ?>
				</a>
			<?php }?>
			<h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
			<?php the_excerpt();?>
		</li>
	<?php endwhile; ?>
</ul>
<a href="index.php?p=11" class="icon more eye">View more ESRC Projects</a> 
