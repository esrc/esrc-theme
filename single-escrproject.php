<?php get_header(); ?>

		<!-- Row for main content area -->
		<div id="content" class="eight columns" role="main">
	
			<div class="post-box">
				<?php if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p class="breadcrumbs">','</p>');
				} ?>
				<?php while (have_posts()) : the_post(); ?>
					<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
						<header>
							<h1 class="entry-title"><?php the_title(); ?></h1>
							<?php if ( has_post_thumbnail() ) {?>
								<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
									<figure class="figure"><?php the_post_thumbnail( array( 'width' => 600, 'height' => 185, 'crop' => 'true' ) , array( 'class' => '' ) ); ?></figure>
								</a>
							<?php }?>
						</header>
						<div class="entry-content">
							<ul class="project-meta no-bullet">
								<li>
									<h3 class="left">Link</h3>
									<div class="meta-content left">
										<p><a href="<?php the_field('project_link'); ?>"><?php the_field('project_link'); ?></a></p>
									</div>
								</li>
								<li>
									<h3 class="left">Partners</h3>
									<div class="meta-content left">
										<?php the_field('project_partners'); ?>
									</div>
								</li>
								<li>
									<h3 class="left">Start</h3>
									 <div class="meta-content left">
										<p><?php $date = DateTime::createFromFormat('Ymd', get_field('project_start_date'));
										echo $date->format('F Y'); ?></p>
									</div>
								</li>
								
								<li>
									<h3 class="left">Staff</h3>
									<div class="meta-content left">
										<?php the_field('project_lead_staff'); ?>
									</div>
								</li>
								<li>
									<h3 class="left">Description</h3>
									<div class="meta-content left">
										<?php the_content(); ?>
									</div>
								</li>
							</ul>
						</div>
						<footer>
							<p><?php the_tags(); ?></p>
						</footer>
					</article>
				<?php endwhile; ?>
			</div>

		</div><!-- End Content row -->
		
		<?php get_sidebar(); ?>
		
<?php get_footer(); ?>