<?php get_header(); ?>

		<!-- Row for main content area -->
		<div id="content" class="eight columns" role="main">
			
			<div class="post-box">
				<?php if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p class="breadcrumbs">','</p>');
				} ?>
				
				<?php while (have_posts()) : the_post(); ?>
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				<?php endwhile; ?>
	
				
				<?php $escr_jobs = new WP_Query( array(
					'post_type' => 'escrjobs'
					));
				?>
				
				<?php while ( $escr_jobs->have_posts() ) : $escr_jobs->the_post(); ?>
					
					<h2><a href="<?php the_field('escr_job_link'); ?>"><?php the_title(); ?></a></h2>
					<ul class="project-meta no-bullet">
						<li>
							<h3>Job Type</h3>
							<p><?php the_field('escr_job_type'); ?></p>
						</li>
						<li>
							<h3>Description</h3>
							<p><?php the_content(); ?></p>
							<p><a href="<?php the_field('escr_job_link'); ?>">Read more</a></p>
						</li>
						
						<li>
							<h3>Closing Date</h3>
							<p><?php $date = DateTime::createFromFormat('Ymd', get_field('escr_job_close_date'));
							echo $date->format('F Y'); ?></p>
						</li>
						<li>
							<h3>Contact Person</h3>
							<p><?php the_field('escr_job_contact'); ?></p>
						</li>
					</ul>
				
				<?php endwhile; ?>
			
			
			</div>
		</div>

<?php get_sidebar(); ?>
		
<?php get_footer(); ?>