<aside id="sidebar" class="four columns" role="complementary">
	<div class="sidebar-box">
	
              <?php get_search_form(); ?>
		<?php if ( is_front_page() ) : ?>
			
			<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Generic Sidebar') ) : ?>
			<?php endif; ?>
	
		<?php elseif ( is_post_type_archive( 'escrproject' ) || 'escrproject' == get_post_type() ) : ?>
		
			<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('ESRC Projects Sidebar') ) : ?>
			<?php endif; ?>
			
		<?php elseif ( is_post_type_archive( 'escrlinkresource' ) || is_post_type_archive( 'esrcguide' ) || is_tax( 'pubtypes' ) ) : ?>
		
			<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Generic Sidebar') ) : ?>
			<?php endif; ?>
			
				<?php elseif ( is_author() ) : ?>
			
			<?php echo dmc_display_author_posts(); ?>
			
		<?php elseif ( is_home() || is_archive() || is_single() ) : ?>
		
			<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('News Sidebar') ) : ?>
			<?php endif; ?>
			
		<?php elseif ( is_tree(7) || is_page( 7 ) || is_page( 81 ) )  : ?>
		<h2>About Us</h2>
			<?php wp_nav_menu( array( 'theme_location' => 'child_navigation', 'container' => false, 'menu_class' => 'child-nav')); ?>
                        			
		<?php elseif ( is_page( 9 ) ) : ?>	
		
			<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Staff Sidebar') ) : ?>
			<?php endif; ?>

                <?php elseif ( is_page( 17 ) ) : ?>	
		
			<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Contact Us Sidebar') ) : ?>
			<?php endif; ?>
			
		<?php elseif ( is_page() ) : ?>
		
			<?php // elseif ( is_page( array( 7 ) ) ) : ?>

			<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Generic Sidebar') ) : ?>
			<?php endif; ?>
		
		<?php endif; ?>
	</div>
</aside>