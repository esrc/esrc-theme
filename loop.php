
<h1>
	<?php if (is_day()) : ?>
		<?php printf(__('Daily Archives: %s', 'reverie'), get_the_date()); ?>
	<?php elseif (is_month()) : ?>
		<?php printf(__('Monthly Archives: %s', 'reverie'), get_the_date('F Y')); ?>
	<?php elseif (is_year()) : ?>
		<?php printf(__('Yearly Archives: %s', 'reverie'), get_the_date('Y')); ?>
	<?php elseif (is_category()) : ?>
		News category: <?php single_cat_title(); ?>
	<?php elseif (is_tag()) : ?>
		<h1>News tagged: <?php single_tag_title(); ?></h1>
	<?php elseif (is_tax( 'projecttypes' )) : ?>
		<h1>ESRC Project category: <?php single_cat_title(); ?></h1>
	<?php elseif (is_tax( 'pubtypes' )) : ?>
		<h1>ESRC Staff Publications category: <?php single_cat_title(); ?></h1>
	<?php elseif (is_home()) : ?>
		<h1>Latest News</h1>
	<?php endif; ?>
</h1>

<?php if (!have_posts()) : ?>
	<div class="notice">
		<p><?php _e('Sorry, no results were found.', 'reverie'); ?></p>
	</div>
<?php endif; ?>

<?php while (have_posts()) : the_post(); ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php if ( has_post_thumbnail() ) {?>
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
				<?php the_post_thumbnail( array( 'width' => 97, 'height' => 97, 'crop' => 'true' ) , array( 'class' => 'alignleft' ) ); ?>
			</a>
		<?php }?>
		<div class="holder">
			<header>
				<?php echo '<time class="updated" datetime="'. get_the_time('c') .'" pubdate>'. sprintf(__('%s'), get_the_time('jS F Y')) .'</time>'; ?>
				<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			</header>
			<div class="entry-content">
				<?php the_excerpt(); ?>
			</div>
			<footer>
				<?php $tag = get_the_tags(); if (!$tag) { } else { ?><p class="tags"><?php the_tags(); ?></p><?php } ?>
			</footer>
		</div>
	</article>	
<?php endwhile; ?>

<?php /* Display navigation to next/previous pages when applicable */ ?>
<?php if ( function_exists('reverie_pagination') ) { reverie_pagination(); } else if ( is_paged() ) { ?>
<nav id="post-nav">
	<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'reverie' ) ); ?></div>
	<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'reverie' ) ); ?></div>
</nav>
<?php } ?>