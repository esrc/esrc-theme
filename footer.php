		</div><!-- End Main row -->
		
		<footer id="content-info" role="contentinfo">
			<div class="row">
				<?php dynamic_sidebar("Footer"); ?>
			</div>
		</footer>
		<footer id="site-footer" >
			<div class="row">
				<div class="six columns">
					<a href="http://www.unimelb.edu.au/"><img alt="logo-footer" src="<?php echo get_template_directory_uri(); ?>/images/logo-footer.png" width="300" height="70" /></a>
				</div>
				<div class="six columns">
					<p>Phone: 13 MELB (13 6352) | International: +(61 3) 9035 5511<br />
					The University of Melbourne ABN: 84 002 705 224<br />
					CRICOS Provider Code: 00116K (<a href="http://services.unimelb.edu.au/international/visas">visa information</a>)<br />
					Web brand guidelines created by <a href="http://marketing.unimelb.edu.au/">Marketing</a></p>
				<ul id="footernav-legals">
				<li><a href="http://www.unimelb.edu.au/disclaimer/">Disclaimer & copyright</a></li>
				<li><a href="http://www.unimelb.edu.au/accessibility/index.html">Accessibility</a></li>
				<li><a href="http://www.unimelb.edu.au/disclaimer/privacy.html">Privacy</a></li>
				</ul>
				</div>
			</div>
		</footer>
			
	</div><!-- Container End -->
	
	<!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
	     chromium.org/developers/how-tos/chrome-frame-getting-started -->
	<!--[if lt IE 7]>
		<script defer src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
		<script defer>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
	<![endif]-->
	
	<?php wp_footer(); ?>
</body>
</html>