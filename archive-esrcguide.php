<?php get_header(); ?>

<!-- Row for main content area -->
<div id="content" class="eight columns" role="main">
			
  <div class="post-box">
    <?php 
      if ( function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb('<p class="breadcrumbs">','</p>');
      }
    ?>
    <h1>ESRC Archival Guides</h1>
     				
    <?php 
      $custom_terms = get_terms('guidetypes');

      foreach($custom_terms as $custom_term) {
        wp_reset_query();
        $args = array('post_type' => 'esrcguide',
          'tax_query' => array(
            array(
              'taxonomy' => 'guidetypes',
              'field' => 'slug',
              'terms' => $custom_term->slug,
            ),
          ),
        );

        $loop = new WP_Query($args);
        if($loop->have_posts()) {
          echo '<h2><small>'.$custom_term->name.'</small></h2>';
          echo '<ul>';
          while($loop->have_posts()) : $loop->the_post();
            echo '<li><a href="'.get_field('esrc_guide_url').'">'.get_the_title().'</a></li>';
          endwhile;
          echo '</ul>';
        }
      }
    ?> 
		
</div>
</div>

<?php get_sidebar(); ?>
		
<?php get_footer(); ?>
