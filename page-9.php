<?php get_header(); ?>

  <!-- Row for main content area -->
  <div id="content" class="eight columns" role="main">
	
    <div class="post-box">
    <?php if ( function_exists('yoast_breadcrumb') ) {
      yoast_breadcrumb('<p class="breadcrumbs">','</p>');
    } ?>
    <h1><?php the_title(); ?></h1>

    <ul class="block-grid one-up">
      <?php 

        // list of users to not display
        $users_to_filter = array("esrc_superuser", "esrc-admin", "dmcdonald", "mkoscott");

        // Create the WP_User_Query object to get a list of user IDs
        $author_query = new WP_User_Query( 
          array(
                'orderby'  => 'meta_value', 
                'meta_key' => 'last_name',
                'fields'   => 'ID'
          ) 
        );

        // Get the list of ID's
        $ids = $author_query->get_results();
	#print '<pre>'; print_r($ids); print '</pre>';

        // foreach ID, get the bits of info we need per user
        //  and populate a data structure - $authors
        $authors = array();
        foreach ($ids as $id) {
          $user_data = get_userdata($id);
          if (! in_array($user_data->user_login, $users_to_filter)) {
            $person = array (
              'id'                   => $id,
              'display_name'         => $user_data->display_name,
              'first_name'           => get_user_meta($id, 'first_name', true),
              'last_name'            => get_user_meta($id, 'last_name', true),
              'user_email'           => $user_data->user_email,
              'esrc_personal_title'  => get_user_meta($id, 'esrc_personal_title', true),
              'esrc_staff_photo'     => get_field('esrc_staff_photo', 'user_' . $id),
              'esrc_staff_bio'       => get_field('esrc_staff_bio', 'user_' . $id),
              'esrc_job_title'       => get_user_meta($id, 'esrc_job_title', true),
              'esrc_phone_number'    => get_user_meta($id, 'esrc_phone_number', true)
            );
            $key = $person['last_name'] . ', ' . $person['first_name'];
            $authors[$key] = $person;
          }
        }

        # find God's (project leader) entry and extract it
        foreach ($authors as $key => $author) {
          if ($key == 'McCarthy, Gavan') {
            $god = $authors[$key];
            unset($authors[$key]);
          };
        }
        # sort the array - ends up by surname
        ksort($authors);
        #print '<pre>'; print_r($authors); print '</pre>';

        # add God back in at the start
	if ( ! empty($god)) {
          array_unshift($authors, $god);
	}
        #print '<pre>'; print_r($authors); print '</pre>';

        # Output results
        foreach( $authors as $author ): ?>
        <li>
          <article>
            <?php if( $author['esrc_staff_photo'] != '' ): ?>
              <img src="<?php echo $author['esrc_staff_photo']; ?>" alt="<?php echo $author['display_name']; ?>" width="100" class="alignleft" />
            <?php else : ?>
              <img src="<?php echo get_template_directory_uri(); ?>/images/no-profilepic.png" alt="No profile photo" title="" width="100" class="alignleft" />
            <?php endif; ?>

            <div class="holder">
						<p class="tags">
                <?php if ($author['esrc_job_title'] != '') { echo $author['esrc_job_title']; }?>
              </p>
              <h2>
                <a href="<?php echo get_author_posts_url( $author['id'] ); ?>"
                   title="More information about this staff member"><?php echo $author['display_name']; ?>
                </a>
              </h2>
              
              <div class="entry-content">
                  <?php if ( $author['esrc_staff_bio']) { echo $author['esrc_staff_bio']; } ?>
		  <?php if( get_field('esrc_phone_number', 'user_' . $author['id'] ) ): ?>
		  <dl class="left">
                      <dt>Phone:</dt>
                      <dd><?php echo $author['esrc_phone_number']; ?></dd>
		  </dl>
		  <?php endif; ?>
		  <dl class="right">
                  <dt> Email:</dt>
                  <?php $nospam = $author['user_email']; ?>
                  <dd><a href="mailto:<?php echo antispambot( $nospam ); ?>"><?php echo antispambot( $nospam ); ?></a></dd>
                </dl>
                <p class="cf">
                  <a href="<?php echo get_author_posts_url( $author['id'] ); ?>" title="More information about this staff member">Read more</a>
                </p>
              </div>
            </div>
          </article>
        </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </div><!-- End Content row -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>
