ESRC Public Web Site, Wordpress Theme
=====================================

Wordpress theme for the eScholarship Research Center public web site.


Change History
--------------

1.1.1

* Migrated theme into new repository on Bitbucket

1.1.0

* Various corrections to formatting throughout


1.0.0
 
* David MacDonald release to ESRC 
