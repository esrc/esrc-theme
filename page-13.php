<?php get_header(); ?>

		<!-- Row for main content area -->
		<div id="content" class="eight columns" role="main">
	
			<div class="post-box">
				<?php if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p class="breadcrumbs">','</p>');
				} ?>
				<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>
				
				<ul class="block-grid one-up">
					<?php $about_children = new WP_Query( array(
						'post_type' => 'esrcpub',
						'meta_key' => 'esrc_feature_pub',
						'meta_value' => true,
						));
					?>
					<?php while ( $about_children->have_posts() ) : $about_children->the_post(); ?>
						<li><article>										
						<?php the_content();?>
                                                <?php if ( get_field('esrc_pub_url') ) : ?>
						<p><a href="<?php the_field('esrc_pub_url'); ?>">More information</a></p>
						<?php endif; ?>
						</article></li>
					<?php endwhile; ?>
				</ul>

			</div>

		</div><!-- End Content row -->
		
		<?php get_sidebar('sidebar-generic'); ?>
		
<?php get_footer(); ?>
