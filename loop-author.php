<?php $curauth = (get_query_var('author_name')) ? get_user_by('slug', get_query_var('author_name')) : get_userdata(get_query_var('author')); ?>

<article id="author-<?php echo $curauth->ID; ?>">
	<?php if( get_field('esrc_staff_photo', 'user_' . $curauth->ID ) ): ?>
		<img src="<?php the_field('esrc_staff_photo', 'user_' . $curauth->ID ); ?>" alt="<?php the_author_meta('display_name', $curauth->ID); ?>" width="100" class="alignleft" />
	<?php else : ?>
		<img src="<?php echo get_template_directory_uri(); ?>/images/no-profilepic.png" alt="No profile photo" title="" width="100" class="alignleft" />
	<?php endif; ?>
	
	<div class="holder">
		<p class="tags"><?php if( get_field('esrc_job_title', 'user_' . $curauth->ID ) ): the_field('esrc_job_title', 'user_' . $curauth->ID ); endif; ?></p>
		<h2><?php echo the_author_meta('display_name', $curauth->ID); ?></h2>
		<div class="entry-content">
			<?php if( get_field('esrc_staff_bio', 'user_' . $curauth->ID ) ): the_field('esrc_staff_bio', 'user_' . $curauth->ID ); endif; ?>
			<?php if( get_field('esrc_phone_number', 'user_' . $curauth->ID ) ): ?>
			<dl class="left">
				<dt>Phone:</dt><dd><?php the_field('esrc_phone_number', 'user_' . $curauth->ID ); ?></dd>
			</dl>
			<?php endif; ?>
			<dl class="right">
				<dt>Email: </dt>
				<?php 
					$nospam = get_the_author_meta('user_email', $curauth->ID);
				?>
				<dd><a href="mailto:<?php echo antispambot( $nospam ); ?>"><?php echo antispambot( $nospam ); ?></a></dd>
			</dl>
		</div>
	</div>
</article>


<?php 

$haspublications = get_field('esrc_staff_academic','user_' . $curauth->ID);

if ($haspublications=='Yes'):

    // display list of publications with coauthors
    if (function_exists('coauthors_posts_links')): 
        $output = "<h3>Selected Publications by ";
        $output .= get_the_author_meta('display_name', $curauth->ID);
        $output .= "</h3>";
        // get the author information
        global $coauthors_plus;
        $user_info = get_userdata($curauth->ID);
        $coauthor = $coauthors_plus->get_coauthor_by('user_login',$user_info->user_login);
        $coauthor_term = $coauthors_plus->get_author_term($coauthor);
        // get the list of co-authored publications
        $args = array(
            'post_type' => 'esrcpub',
            'posts_per_page' => -1,
            'post_status' => 'publish',
            'tax_query' => array(
                array(
                    'taxonomy' => 'author',
                    'field' => 'slug', 
                    'terms' => $coauthor_term->slug,
                ),      
            ),  
        );      
        $author_query = new WP_Query($args);

    // present the list of publications
    $output .= "<ul class='block-grid one-up'>";
    if ($author_query->have_posts()) :
        while ($author_query->have_posts()) : 
            $author_query->the_post();
            $output .= "<li><article>";
            if (get_field('esrc_pub_url')):
                $output .= "<h3><a href='" . get_field('esrc_pub_url') . "'>" . get_the_title() . "</a></h3>";
            else:
                $output .= "<h3>" . get_the_title() . "</h3>";
            endif;
            $output .= get_the_content();
            $output .= "</article></li>";
        endwhile;
    endif; 
    $output .= "</ul>";
    // write content
    echo $output;
endif;

endif;

?>

