<?php get_header(); ?>

		<!-- Row for main content area -->
		<div id="content" class="eight columns" role="main">
			
			<div class="project-intro">
				<?php if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p class="breadcrumbs">','</p>');
				} ?>
				<h1>Hello</h1>
				<p><?php the_content(); ?></p>
			</div>
			
			
			<div class="post-box six columns">
			
				<?php while (have_posts()) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<?php if ( has_post_thumbnail() ) {?>
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
								<?php the_post_thumbnail( array( 'width' => 87, 'height' => 87, 'crop' => 'true' ) , array( 'class' => 'alignleft' ) ); ?>
							</a>
						<?php }?>
						<div class="holder">
							<header>
								<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
							</header>
							<div class="entry-content">
								<?php the_excerpt(); ?>
							</div>
							
						</div>
					</article>	
				<?php endwhile; ?>
				
			</div>
			
			

		</div><!-- End Content row -->
		
		<?php get_sidebar(); ?>
		
<?php get_footer(); ?>