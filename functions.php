<?php
function reverie_setup() {
	// Add language supports.
	load_theme_textdomain('reverie', get_template_directory() . '/lang');
	
	// Add post thumbnail supports. http://codex.wordpress.org/Post_Thumbnails
	add_theme_support('post-thumbnails');
	// set_post_thumbnail_size (97, 97, false);
	
	add_post_type_support( 'page', 'excerpt' );
	
	// Add menu supports. http://codex.wordpress.org/Function_Reference/register_nav_menus
	add_theme_support('menus');
	register_nav_menus(array(
		'primary_navigation' => __('Primary Navigation', 'reverie'),
		'global_navigation' => __('Global Navigation', 'reverie'),
		'child_navigation' => __('Child Navigation', 'reverie'),
		'footer_navigation' => __('Footer Navigation', 'reverie')
	));	
}
add_action('after_setup_theme', 'reverie_setup');

// Enqueue for header and footer, thanks to flickapix on Github.
// Enqueue css files
function reverie_css() {
  if ( !is_admin() ) {
  
     wp_register_style( 'foundation',get_template_directory_uri() . '/css/foundation.css', false );
     wp_enqueue_style( 'foundation' );
    
     wp_register_style( 'app',get_template_directory_uri() . '/css/app.css', false );
     wp_enqueue_style( 'app' );
     
     // Load style.css to allow contents overwrite foundation & app css
     wp_register_style( 'style',get_template_directory_uri() . '/style.css', false );
     wp_enqueue_style( 'style' );
     
  }
}  
add_action( 'init', 'reverie_css' );

function reverie_ie_css () {
    echo '<!--[if lt IE 9]>';
    echo '<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/ie.css">';
    echo '<![endif]-->';
}
add_action( 'wp_head', 'reverie_ie_css' );

// Enqueue js files
function reverie_scripts() {

global $is_IE;

  if ( !is_admin() ) {
  
  // Enqueue to header
     wp_deregister_script( 'jquery' );
     wp_register_script( 'jquery', get_template_directory_uri() . '/js/jquery.min.js' );
     wp_enqueue_script( 'jquery' );
     
     wp_register_script( 'modernizr', get_template_directory_uri() . '/js/modernizr.foundation.js', array( 'jquery' ) );
     wp_enqueue_script( 'modernizr' );
 
  // Enqueue to footer
     wp_register_script( 'reveal', get_template_directory_uri() . '/js/jquery.reveal.js', array( 'jquery' ), false, true );
     wp_enqueue_script( 'reveal' );
     
     wp_register_script( 'orbit', get_template_directory_uri() . '/js/jquery.orbit-1.4.0.js', array( 'jquery' ), false, true );
     wp_enqueue_script( 'orbit' );
     
     wp_register_script( 'custom_forms', get_template_directory_uri() . '/js/jquery.customforms.js', array( 'jquery' ), false, true );
     wp_enqueue_script( 'custom_forms' );
     
     wp_register_script( 'placeholder', get_template_directory_uri() . '/js/jquery.placeholder.min.js', array( 'jquery' ), false, true );
     wp_enqueue_script( 'placeholder' );
     
     wp_register_script( 'tooltips', get_template_directory_uri() . '/js/jquery.tooltips.js', array( 'jquery' ), false, true );
     wp_enqueue_script( 'tooltips' );
     
     wp_register_script( 'app', get_template_directory_uri() . '/js/app.js', array( 'jquery' ), false, true );
     wp_enqueue_script( 'app' );
     
    
     if ($is_IE) {
        wp_register_script ( 'html5shiv', "http://html5shiv.googlecode.com/svn/trunk/html5.js" , false, true);
        wp_enqueue_script ( 'html5shiv' );
     } 
     
     // Enable threaded comments 
     if ( (!is_admin()) && is_singular() && comments_open() && get_option('thread_comments') )
		wp_enqueue_script('comment-reply');
  }
}
add_action( 'init', 'reverie_scripts' );

if (function_exists('register_sidebar')) {

	// Homepage Sidebars
	register_sidebar(array(
		'name'=> 'Generic Sidebar',
		'id' => 'sidebar-generic',
		'before_widget' => '<article id="%1$s" class="row widget %2$s"><div class="sidebar-section twelve columns">',
		'after_widget' => '</div></article>',
		'before_title' => '<h2>',
		'after_title' => '</h2>'
	));
	
	register_sidebar(array(
		'name'=> 'About Sidebar',
		'id' => 'sidebar-about',
                'before_widget' => '<article id="%1$s" class="row widget %2$s"><div class="sidebar-section ten columns">',
		'after_widget' => '</div></article>',
		'before_title' => '<h2>',
		'after_title' => '</h2>'
	));
	
		register_sidebar(array(
		'name'=> 'ESRC Projects Sidebar',
		'id' => 'sidebar-projects',
		'before_widget' => '<article id="%1$s" class="row widget %2$s"><div class="sidebar-section ten columns">',
		'after_widget' => '</div></article>',
		'before_title' => '<h2>',
		'after_title' => '</h2>'
	));

                register_sidebar(array(
		'name'=> 'Staff Sidebar',
		'id' => 'sidebar-staff',
		'before_widget' => '<article id="%1$s" class="row widget %2$s"><div class="sidebar-section ten columns">',
		'after_widget' => '</div></article>',
		'before_title' => '<h2>',
		'after_title' => '</h2>'
	));
	
        register_sidebar(array(
		'name'=> 'Publications Sidebar',
		'id' => 'sidebar-publications',
		'before_widget' => '<article id="%1$s" class="row widget %2$s"><div class="sidebar-section twelve columns">',
		'after_widget' => '</div></article>',
		'before_title' => '<h2>',
		'after_title' => '</h2>'
	));

	register_sidebar(array(
		'name'=> 'News Sidebar',
		'id' => 'sidebar-news',
		'before_widget' => '<article id="%1$s" class="row widget %2$s"><div class="sidebar-section twelve columns">',
		'after_widget' => '</div></article>',
		'before_title' => '<h2>',
		'after_title' => '</h2>'
	));

         register_sidebar(array(
		'name'=> 'Contact Us Sidebar',
		'id' => 'sidebar-contact',
		'before_widget' => '<article id="%1$s" class="row widget %2$s"><div class="sidebar-section twelve columns">',
		'after_widget' => '</div></article>',
		'before_title' => '<h2>',
		'after_title' => '</h2>'
	));
	
	register_sidebar(array(
		'name'=> 'Footer',
		'id' => 'sidebar-footer',
		'before_widget' => '<article id="%1$s" class="four columns widget %2$s"><div class="footer-section">',
		'after_widget' => '</div></article>',
		'before_title' => '<h5>',
		'after_title' => '</h5>'
	));
	
}

// Check if page is a child

function is_tree( $pid ) {      
    global $post;               
    if ( is_page($pid) )
        return true;            
    $anc = get_post_ancestors( $post->ID );
    foreach ( $anc as $ancestor ) {
        if( is_page() && $ancestor == $pid ) {
            return true;
        }
    }
    return false; 
}

// Create thumbnail image sizes
add_image_size( 'thumb-wide', 190, 97, true );
add_image_size( 'thumb-medium', 97, 97, true );
add_image_size( 'thumb-small', 87, 87, true );

// Remove obsolete user contact info and add new
function dmc_new_contactmethods( $contactmethods ) {

// Remove these
unset($contactmethods['aim']);
unset($contactmethods['yim']);
unset($contactmethods['jabber']);
unset($contactmethods['description']);

// Add Twitter
$contactmethods['twitter'] = 'Twitter';

    return $contactmethods;
}
add_filter( 'user_contactmethods', 'dmc_new_contactmethods',10,1 );

// Remove default user bio field
function dmc_enqueue_scripts($hook){

    $screen = get_current_screen();
    // Check screen hook and current post type
    if ( 'user-edit.php' == $hook || 'profile.php' == $hook ){
        wp_register_script( 'remove_user_bio', get_template_directory_uri() . '/js/remove-user-bio.js', array( 'jquery' ), false, true );
      	wp_enqueue_script( 'remove_user_bio' );
    }
}
add_action( 'admin_enqueue_scripts', 'dmc_enqueue_scripts' ,10,1);

// Remove unwanted dashboard items
function dmc_custom_dashboard_widgets() {
	global $wp_meta_boxes;

	//QuickPress
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
	//Wordpress Development Blog Feed
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
	//Other Wordpress News Feed
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
	//Plugins - Popular, New and Recently updated Wordpress Plugins
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
}
add_action('wp_dashboard_setup', 'dmc_custom_dashboard_widgets');


// display author posts.
function dmc_display_author_posts() {
    global $post, $curauth;
	$curauth = (get_query_var('author_name')) ? get_user_by('slug', get_query_var('author_name')) : get_userdata(get_query_var('author'));
	
    $authors_posts = get_posts( array( 'author' => $curauth->ID ) ); ?>
	
	<h3>News Posts by <?php echo $curauth->display_name; ?>:</h3>
	
	<?php if ( $authors_posts ) {
	
    $output = '<ul>';
    foreach ( $authors_posts as $authors_post ) {
        $output .= '<li><a href="' . get_permalink( $authors_post->ID ) . '">' . apply_filters( 'the_title', $authors_post->post_title, $authors_post->ID ) . '</a></li>';
    }
    $output .= '</ul>';

    return $output;
    
    } else echo '<p>There are currently no news posts by this staff member.</p>';
}


// return entry meta information for posts, used by multiple loops.
function reverie_entry_meta() {
	echo '<time class="updated" datetime="'. get_the_time('c') .'" pubdate>'. sprintf(__('Posted on %s at %s.', 'reverie'), get_the_time('l, F jS, Y'), get_the_time()) .'</time>';
	if ( function_exists( 'coauthors_posts_links' ) ) {
    coauthors_posts_links();
	} else {
   the_author_posts_link();
	}
echo '<p class="byline author vcard">'. __('By', 'reverie') .' <a href="'. get_author_posts_url(get_the_author_meta('ID')) .'" rel="author" class="fn">'. get_the_author() .'</a></p>';
}
// return year and month meta.
function esrc_entry_meta() {
	echo '<time class="updated" datetime="'. get_the_time('c') .'" pubdate>'. sprintf(__('%s', 'reverie'), get_the_time('jS F Y')) .'</time>';
}

/* Customized the output of caption, you can remove the filter to restore back to the WP default output. Courtesy of DevPress. http://devpress.com/blog/captions-in-wordpress/ */
add_filter( 'img_caption_shortcode', 'cleaner_caption', 10, 3 );

function cleaner_caption( $output, $attr, $content ) {

	/* We're not worried abut captions in feeds, so just return the output here. */
	if ( is_feed() )
		return $output;

	/* Set up the default arguments. */
	$defaults = array(
		'id' => '',
		'align' => 'alignnone',
		'width' => '',
		'caption' => ''
	);

	/* Merge the defaults with user input. */
	$attr = shortcode_atts( $defaults, $attr );

	/* If the width is less than 1 or there is no caption, return the content wrapped between the [caption]< tags. */
	if ( 1 > $attr['width'] || empty( $attr['caption'] ) )
		return $content;

	/* Set up the attributes for the caption <div>. */
	$attributes = ' class="figure ' . esc_attr( $attr['align'] ) . '"';

	/* Open the caption <div>. */
	$output = '<figure' . $attributes .'>';

	/* Allow shortcodes for the content the caption was created for. */
	$output .= do_shortcode( $content );

	/* Append the caption text. */
	$output .= '<figcaption>' . $attr['caption'] . '</figcaption>';

	/* Close the caption </div>. */
	$output .= '</figure>';

	/* Return the formatted, clean caption. */
	return $output;
}

// Clean the output of attributes of images in editor. Courtesy of SitePoint. http://www.sitepoint.com/wordpress-change-img-tag-html/
function image_tag_class($class, $id, $align, $size) {
	$align = 'align' . esc_attr($align);
	return $align;
}
add_filter('get_image_tag_class', 'image_tag_class', 0, 4);
function image_tag($html, $id, $alt, $title) {
	return preg_replace(array(
			'/\s+width="\d+"/i',
			'/\s+height="\d+"/i',
			'/alt=""/i'
		),
		array(
			'',
			'',
			'',
			'alt="' . $title . '"'
		),
		$html);
}
add_filter('get_image_tag', 'image_tag', 0, 4);

// Customize output for menu
class reverie_walker extends Walker_Nav_Menu {
  function start_lvl(&$output, $depth) {
    $indent = str_repeat("\t", $depth);
    $output .= "\n$indent<a href=\"#\" class=\"flyout-toggle\"><span> </span></a><ul class=\"flyout\">\n";
  }
}

// Add Foundation 'active' class for the current menu item 
function reverie_active_nav_class( $classes, $item )
{
    if($item->current == 1)
    {
        $classes[] = 'active';
    }
    return $classes;
}
add_filter( 'nav_menu_css_class', 'reverie_active_nav_class', 10, 2 );

// img unautop, Courtesy of Interconnectit http://interconnectit.com/2175/how-to-remove-p-tags-from-images-in-wordpress/
function img_unautop($pee) {
    $pee = preg_replace('/<p>\\s*?(<a .*?><img.*?><\\/a>|<img.*?>)?\\s*<\\/p>/s', '<figure>$1</figure>', $pee);
    return $pee;
}
add_filter( 'the_content', 'img_unautop', 30 );

// Homepage Slider

add_action('init', 'register_dmc_orbit_slider');

function register_dmc_orbit_slider() {
 
	$labels = array(
		'name' => _x( 'Homepage Slider', 'post type general name' ),
		'singular_name' => _x( 'Homepage Slider', 'post type singular name' ),
		'add_new' => _x( 'Add New', 'Homepage Slider' ),
		'add_new_item' => __( 'Add New Homepage Slider' ),
		'edit_item' => __( 'Edit Homepage Slider' ),
		'new_item' => __( 'New Homepage Slider' ),
		'all_items' => __( 'All Homepage Sliders' ),
		'view_item' => __( 'View Homepage Slider' ),
		'search_items' => __( 'Search Homepage Sliders' ),
		'not_found' =>  __( 'No Homepage Sliders found' ),
		'not_found_in_trash' => __( 'No Homepage Sliders found in Trash' ),
		'parent_item_colon' => '',
		'menu_name' => 'Homepage Slider'
	
	  );
	  $args = array(
		'labels' => $labels,
		'public' => true,
		'query_var' => true,
		'rewrite' => true,
		// 'capability_type' => 'post',
		'capabilities' => array(
			'publish_posts' => 'manage_options',
			'edit_posts' => 'manage_options',
			'edit_others_posts' => 'manage_options',
			'delete_posts' => 'manage_options',
			'delete_others_posts' => 'manage_options',
			'read_private_posts' => 'manage_options',
			'edit_post' => 'manage_options',
			'delete_post' => 'manage_options',
			'read_post' => 'manage_options',
    	),
		'has_archive' => true,
		'hierarchical' => false,
		'menu_position' => 5,
		'rewrite' => array( 'slug' => 'slider', 'with_front' => false ),
		'supports' => array( 'title', 'editor', 'thumbnail' )
	  );
	 
	register_post_type( 'orbit_slider' , $args );
}

function SliderContent(){

	$args = array( 'post_type' => 'orbit_slider', 'order' => 'ASC' );
	$loop = new WP_Query( $args );
	
	while ( $loop->have_posts() ) : $loop->the_post();
	
		$slider_link = get_field('slider_image_links_to');
		foreach ( $slider_link as $post_object ): ?>
	
			<?php echo '<a href="'.get_permalink($post_object->ID) .'">';
				if ( has_post_thumbnail() ) {  the_post_thumbnail( 'slider-image-large' ); } ?>
				<div class="orbit-caption">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
			<?php echo '</a>'; ?>
			
		<?php endforeach; 
	endwhile;
}
// Remove menu item if not admin
function wpse28782_remove_menu_items() {
    if( !current_user_can( 'administrator' ) ):
        remove_menu_page( 'edit.php?post_type=orbit_slider' );
    endif;
}
add_action( 'admin_menu', 'wpse28782_remove_menu_items' );


// Pagination
function reverie_pagination() {
	global $wp_query;
 
	$big = 999999999; // This needs to be an unlikely integer
 
	// For more options and info view the docs for paginate_links()
	// http://codex.wordpress.org/Function_Reference/paginate_links
	$paginate_links = paginate_links( array(
		'base' => str_replace( $big, '%#%', get_pagenum_link($big) ),
		'current' => max( 1, get_query_var('paged') ),
		'total' => $wp_query->max_num_pages,
		'mid_size' => 5,
		'prev_next' => True,
	    'prev_text' => __('&laquo;'),
	    'next_text' => __('&raquo;'),
		'type' => 'list'
	) );
 
	// Display the pagination if more than one page is found
	if ( $paginate_links ) {
		echo '<div class="reverie-pagination">';
		echo $paginate_links;
		echo '</div><!--// end .pagination -->';
	}
}

function esrc_pubtypes_cat(){
	global $post; 
	$post_terms = wp_get_object_terms($post->ID, 'pubtypes');
	if(!empty($post_terms)){
	  if(!is_wp_error( $post_terms )){
                $output ='';
                $output.= 'Category: ';	  	
		foreach($post_terms as $term){
		$output.= '<a href="'.get_term_link($term->slug, 'pubtypes').'">'.$term->name.'</a>, '; 
                }
		
		return $output;
	  }
	}
}

// ESRC Project Types taxonomy
add_action( 'init', 'register_taxonomy_projecttypes' );

function register_taxonomy_projecttypes() {

    $labels = array( 
        'name' => _x( 'ESRC Project Types', 'projecttypes' ),
        'singular_name' => _x( 'ESRC Project Type', 'projecttypes' ),
        'search_items' => _x( 'Search ESRC Project Types', 'projecttypes' ),
        'popular_items' => _x( 'Popular ESRC Project Types', 'projecttypes' ),
        'all_items' => _x( 'All ESRC Project Types', 'projecttypes' ),
        'parent_item' => _x( 'Parent ESRC Project Type', 'projecttypes' ),
        'parent_item_colon' => _x( 'Parent ESRC Project Type:', 'projecttypes' ),
        'edit_item' => _x( 'Edit ESRC Project Type', 'projecttypes' ),
        'update_item' => _x( 'Update ESRC Project Type', 'projecttypes' ),
        'add_new_item' => _x( 'Add New ESRC Project Type', 'projecttypes' ),
        'new_item_name' => _x( 'New ESRC Project Type', 'projecttypes' ),
        'separate_items_with_commas' => _x( 'Separate ESRC Project Types with commas', 'projecttypes' ),
        'add_or_remove_items' => _x( 'Add or remove ESRC Project Types', 'projecttypes' ),
        'choose_from_most_used' => _x( 'Choose from the most used ESRC Project Types', 'projecttypes' ),
        'menu_name' => _x( 'ESRC Project Types', 'projecttypes' ),
    );

    $args = array( 
        'labels' => $labels,
        'public' => true,
        'show_in_nav_menus' => true,
        'show_ui' => true,
        'show_tagcloud' => false,
        'hierarchical' => true,
        'rewrite' => true,
        'query_var' => true
    );

    register_taxonomy( 'projecttypes', array('escrproject'), $args );
}

// ESRC Project (label) & escrproject (registered custom post type name)
add_action( 'init', 'escrproject_custom_post_type' );
function escrproject_custom_post_type() {

$labels = array(
    'name' => _x('ESRC Projects', 'post type general name'),
    'singular_name' => _x('ESRC Project', 'post type singular name'),
    'add_new' => _x('Add New ESRC Project', 'ESRC Project'),
    'add_new_item' => __('Add New ESRC Project'),
    'edit_item' => __('Edit ESRC Project'),
    'new_item' => __('New ESRC Project'),
    'all_items' => __('All ESRC Projects'),
    'view_item' => __('View ESRC Project'),
    'search_items' => __('Search ESRC Projects'),
    'not_found' =>  __('No ESRC Projects found'),
    'not_found_in_trash' => __('No ESRC Projects found in Trash'),
    'parent_item_colon' => '',
    'menu_name' => 'ESRC Projects'

  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    //'show_in_menu' => 'edit.php?post_type=book',
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'has_archive' => true,
    'hierarchical' => false,
    'can_export' => true,
    'show_in_nav_menus' => true,
    'taxonomies' => array('post_tag') ,
    'rewrite' => array( 'slug' => 'projects', 'with_front' => false ),
    'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail' )
  );
  
  //$taxs = array();
    
  register_post_type('escrproject', $args);
}


// Links & Resources Types taxonomy
add_action( 'init', 'register_taxonomy_linktypes' );

function register_taxonomy_linktypes() {

    $labels = array( 
        'name' => _x( 'Links & Resources Categories', 'linktypes' ),
        'singular_name' => _x( 'Links & Resources Category', 'linktypes' ),
        'search_items' => _x( 'Search Links & Resources Categories', 'linktypes' ),
        'popular_items' => _x( 'Popular Links & Resources Categories', 'linktypes' ),
        'all_items' => _x( 'All Links & Resources Categories', 'linktypes' ),
        'parent_item' => _x( 'Parent Links & Resources Category', 'linktypes' ),
        'parent_item_colon' => _x( 'Parent Links & Resources Category:', 'linktypes' ),
        'edit_item' => _x( 'Edit Links & Resources Category', 'linktypes' ),
        'update_item' => _x( 'Update Links & Resources Category', 'linktypes' ),
        'add_new_item' => _x( 'Add New Links & Resources Category', 'linktypes' ),
        'new_item_name' => _x( 'New Links & Resources Category', 'linktypes' ),
        'separate_items_with_commas' => _x( 'Separate Links & Resources Categories with commas', 'linktypes' ),
        'add_or_remove_items' => _x( 'Add or remove Links & Resources Categories', 'linktypes' ),
        'choose_from_most_used' => _x( 'Choose from the most used Links & Resources Categories', 'linktypes' ),
        'menu_name' => _x( 'Links & Resources Categories', 'linktypes' ),
    );

    $args = array( 
        'labels' => $labels,
        'public' => true,
        'show_in_nav_menus' => true,
        'show_ui' => true,
        'show_tagcloud' => false,
        'hierarchical' => true,
        'rewrite' => true,
        'query_var' => true
    );

    register_taxonomy( 'linktypes', array('escrlinkresource'), $args );
}

// ESRC Links or Resources (label) & linkresource (registered custom post type name)
add_action( 'init', 'escrlinkresource_custom_post_type' );
function escrlinkresource_custom_post_type() {

$labels = array(
    'name' => _x('ESRC Links or Resources', 'post type general name'),
    'singular_name' => _x('ESRC Link or Resource', 'post type singular name'),
    'add_new' => _x('Add New ESRC Link or Resource', 'ESRC Link or Resource'),
    'add_new_item' => __('Add New ESRC Link or Resource'),
    'edit_item' => __('Edit ESRC Link or Resource'),
    'new_item' => __('New ESRC Link or Resource'),
    'all_items' => __('All ESRC Links or Resources'),
    'view_item' => __('View ESRC Link or Resource'),
    'search_items' => __('Search ESRC Links or Resources'),
    'not_found' =>  __('No ESRC Links or Resources found'),
    'not_found_in_trash' => __('No ESRC Links or Resources found in Trash'),
    'parent_item_colon' => '',
    'menu_name' => 'ESRC Links or Resources'

  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    //'show_in_menu' => 'edit.php?post_type=book',
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'has_archive' => true,
    'hierarchical' => false,
    'can_export' => true,
    'show_in_nav_menus' => true,
    //'taxonomies' => $taxs,
    'rewrite' => array( 'slug' => 'links', 'with_front' => false ),
    'supports' => array( 'title' )
  );
  
  //$taxs = array();
    
  register_post_type('escrlinkresource', $args);
}

// ESRC Archival Guides taxonomy
add_action( 'init', 'register_taxonomy_guidetypes' );

function register_taxonomy_guidetypes() {

    $labels = array( 
        'name' => _x( 'Archival Guides Categories', 'linktypes' ),
        'singular_name' => _x( 'Archival Guide Category', 'linktypes' ),
        'search_items' => _x( 'Search Archival Guides Categories', 'linktypes' ),
        'popular_items' => _x( 'Popular Archival Guides Categories', 'linktypes' ),
        'all_items' => _x( 'All Archival Guides Categories', 'linktypes' ),
        'parent_item' => _x( 'Parent Archival Guide Category', 'linktypes' ),
        'parent_item_colon' => _x( 'Parent Archival Guide Category:', 'linktypes' ),
        'edit_item' => _x( 'Edit Archival Guide Category', 'linktypes' ),
        'update_item' => _x( 'Update Archival Guide Category', 'linktypes' ),
        'add_new_item' => _x( 'Add New Archival Guide Category', 'linktypes' ),
        'new_item_name' => _x( 'New Archival Guides Category', 'linktypes' ),
        'separate_items_with_commas' => _x( 'Separate Archival Guides Categories with commas', 'linktypes' ),
        'add_or_remove_items' => _x( 'Add or remove Archival Guide Categories', 'linktypes' ),
        'choose_from_most_used' => _x( 'Choose from the most used Archival Guide Categories', 'linktypes' ),
        'menu_name' => _x( 'Archival Guides Categories', 'linktypes' ),
    );

    $args = array( 
        'labels' => $labels,
        'public' => true,
        'show_in_nav_menus' => true,
        'show_ui' => true,
        'show_tagcloud' => false,
        'hierarchical' => true,
        'rewrite' => true,
        'query_var' => true
    );

    register_taxonomy( 'guidetypes', array('esrcguide'), $args );
}

// ESRC Archival Guides
add_action( 'init', 'esrc_archival_guides_custom_post_type' );
function esrc_archival_guides_custom_post_type() {

$labels = array(
    'name' => _x('ESRC Archival Guides', 'post type general name'),
    'singular_name' => _x('ESRC Archival Guide', 'post type singular name'),
    'add_new' => _x('Add New ESRC Archival Guide', 'ESRC Link or Resource'),
    'add_new_item' => __('Add New ESRC Archival Guide'),
    'edit_item' => __('Edit ESRC Archival Guide'),
    'new_item' => __('New ESRC Archival Guide'),
    'all_items' => __('All ESRC Archival Guides'),
    'view_item' => __('View ESRC Archival Guide'),
    'search_items' => __('Search ESRC Archival Guides'),
    'not_found' =>  __('No ESRC Archival Guides found'),
    'not_found_in_trash' => __('No ESRC Archival Guides found in Trash'),
    'parent_item_colon' => '',
    'menu_name' => 'ESRC Archival Guides'

  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    //'show_in_menu' => 'edit.php?post_type=book',
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'has_archive' => true,
    'hierarchical' => false,
    'can_export' => true,
    'show_in_nav_menus' => true,
    //'taxonomies' => $taxs,
    'rewrite' => array( 'slug' => 'archival-guides', 'with_front' => false ),
    'supports' => array( 'title' )
  );
  
  //$taxs = array();
    
  register_post_type('esrcguide', $args);
}

// ESRC Employment (label) & jobs (registered custom post type name)
add_action( 'init', 'escrjobs_custom_post_type' );
function escrjobs_custom_post_type() {

$labels = array(
    'name' => _x('ESRC Employment', 'post type general name'),
    'singular_name' => _x('ESRC Employment Post', 'post type singular name'),
    'add_new' => _x('Add New ESRC Employment Post', 'ESRC Employment Post'),
    'add_new_item' => __('Add New ESRC Employment Post'),
    'edit_item' => __('Edit ESRC Employment Post'),
    'new_item' => __('New ESRC Employment Post'),
    'all_items' => __('All ESRC Employment Posts'),
    'view_item' => __('View ESRC Employment Post'),
    'search_items' => __('Search ESRC Employment Posts'),
    'not_found' =>  __('No ESRC Employment Posts found'),
    'not_found_in_trash' => __('No ESRC Employment Posts found in Trash'),
    'parent_item_colon' => '',
    'menu_name' => 'ESRC Employment'

  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    //'show_in_menu' => 'edit.php?post_type=book',
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'has_archive' => true,
    'hierarchical' => false,
    'can_export' => true,
    'show_in_nav_menus' => true,
    //'taxonomies' => $taxs,
    'rewrite' => array( 'slug' => 'jobs', 'with_front' => false ),
    'supports' => array( 'title', 'editor' )
  );
  
  //$taxs = array();
    
  register_post_type('escrjobs', $args);
}

// ESRC Staff Publications taxonomy
add_action( 'init', 'register_taxonomy_pubtypes' );

function register_taxonomy_pubtypes() {

    $labels = array( 
        'name' => _x( 'Staff Publications Categories', 'linktypes' ),
        'singular_name' => _x( 'Staff Publications Category', 'linktypes' ),
        'search_items' => _x( 'Search Staff Publications Categories', 'linktypes' ),
        'popular_items' => _x( 'Popular Staff Publications Categories', 'linktypes' ),
        'all_items' => _x( 'All Staff Publications Categories', 'linktypes' ),
        'parent_item' => _x( 'Parent Staff Publications Category', 'linktypes' ),
        'parent_item_colon' => _x( 'Parent Staff Publications Category:', 'linktypes' ),
        'edit_item' => _x( 'Edit Staff Publications Category', 'linktypes' ),
        'update_item' => _x( 'Update Staff Publications Category', 'linktypes' ),
        'add_new_item' => _x( 'Add New Staff Publications Category', 'linktypes' ),
        'new_item_name' => _x( 'New Staff Publications Category', 'linktypes' ),
        'separate_items_with_commas' => _x( 'Separate Staff Publications Categories with commas', 'linktypes' ),
        'add_or_remove_items' => _x( 'Add or remove Staff Publications Categories', 'linktypes' ),
        'choose_from_most_used' => _x( 'Choose from the most used Staff Publications Categories', 'linktypes' ),
        'menu_name' => _x( 'Staff Publications Categories', 'linktypes' ),
    );

    $args = array( 
        'labels' => $labels,
        'public' => true,
        'show_in_nav_menus' => true,
        'show_ui' => true,
        'show_tagcloud' => false,
        'hierarchical' => true,
        'rewrite' => true,
        'query_var' => true
    );

    register_taxonomy( 'pubtypes', array('esrcpub'), $args );
}

// ESRC Staff Publications
add_action( 'init', 'esrc_staff_pubs_custom_post_type' );
function esrc_staff_pubs_custom_post_type() {

$labels = array(
    'name' => _x('ESRC Staff Publications', 'post type general name'),
    'singular_name' => _x('ESRC Staff Publication', 'post type singular name'),
    'add_new' => _x('Add New ESRC Staff Publication', 'ESRC Link or Resource'),
    'add_new_item' => __('Add New ESRC Staff Publication'),
    'edit_item' => __('Edit ESRC Staff Publication'),
    'new_item' => __('New ESRC Staff Publication'),
    'all_items' => __('All ESRC Staff Publications'),
    'view_item' => __('View ESRC Staff Publication'),
    'search_items' => __('Search ESRC Staff Publications'),
    'not_found' =>  __('No ESRC Staff Publications found'),
    'not_found_in_trash' => __('No ESRC Staff Publications found in Trash'),
    'parent_item_colon' => '',
    'menu_name' => 'ESRC Staff Publications'

  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    //'show_in_menu' => 'edit.php?post_type=book',
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'has_archive' => true,
    'hierarchical' => false,
    'can_export' => true,
    'show_in_nav_menus' => true,
    //'taxonomies' => $taxs,
    'rewrite' => array( 'slug' => 'staff-pubs', 'with_front' => false ),
    'supports' => array( 'title', 'editor', 'author' )
  );
  
  //$taxs = array();
    
  register_post_type('esrcpub', $args);
}

?>