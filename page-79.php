<?php get_header(); ?>

		<!-- Row for main content area -->
		<div id="content" class="eight columns" role="main">
			
			<div class="post-box">
				<?php if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p class="breadcrumbs">','</p>');
				} ?>
				
				<?php while (have_posts()) : the_post(); ?>
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				<?php endwhile; ?>
	
				<?php 
					$custom_terms = get_terms('linktypes');
					
					foreach($custom_terms as $custom_term) {
						wp_reset_query();
						$args = array('post_type' => 'escrlinkresource',
							'tax_query' => array(
								array(
									'taxonomy' => 'linktypes',
									'field' => 'slug',
									'terms' => $custom_term->slug,
								),
							),
						 );
					
						 $loop = new WP_Query($args);
						 if($loop->have_posts()) {
							echo '<h2><small>'.$custom_term->name.'</small></h2>';
							echo '<ul class="list">';
							while($loop->have_posts()) : $loop->the_post();
								echo '<li><a href="'.get_field('esrc_link_url').'">'.get_the_title().'</a></li>';
							endwhile;
							echo '</ul>';
						 }
					}
				?> 
			
			</div>
		</div>

<?php get_sidebar(); ?>
		
<?php get_footer(); ?>